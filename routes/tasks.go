package routes

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/marlondev/task-api/database"
)

type Task struct {
	ID          string `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
}

func GetTasks(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	connection := database.CreateConnection()
	defer connection.Close()

	var tasks []Task

	rows, err := connection.Query("select * from task")

	if err != nil {
		fmt.Println("Error to fetch", err)
	}

	for rows.Next() {
		var task Task

		err = rows.Scan(&task.ID, &task.Title, &task.Description)
		tasks = append(tasks, task)
	}

	json.NewEncoder(w).Encode(tasks)

}

func GetTask(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	connection := database.CreateConnection()
	defer connection.Close()
	var task Task

	params := mux.Vars(r)
	id := params["id"]

	row := connection.QueryRow("select * from task where id=" + id)
	row.Scan(&task.ID, &task.Title, &task.Description)
	json.NewEncoder(w).Encode(task)

}

func CreateTask(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	connection := database.CreateConnection()
	defer connection.Close()

	var task Task

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprint(w, "Bad Request")
	}

	json.Unmarshal(body, &task)

	result, err := connection.Exec("insert into task (title, description) values(?, ?)", task.Title, task.Description)
	index, _ := result.LastInsertId()
	task.ID = strconv.FormatInt(index, 10)
	json.NewEncoder(w).Encode(task)

}

func UpdateTask(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(http.StatusCreated)
	connection := database.CreateConnection()
	defer connection.Close()

	var task Task

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprint(w, "Bad Request")
	}

	params := mux.Vars(r)
	id := params["id"]

	json.Unmarshal(body, &task)

	_, err = connection.Exec("update task set title=? where id="+id, task.Title)
	_, err = connection.Exec("update task set description=? where id="+id, task.Description)

	task.ID = id
	json.NewEncoder(w).Encode(task)

}

func DeleteTask(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	connection := database.CreateConnection()
	defer connection.Close()

	params := mux.Vars(r)
	id := params["id"]

	_, err := connection.Exec("delete from task where id=" + id)
	if err != nil {
		fmt.Fprint(w, "false")
	} else {
		fmt.Fprint(w, "true")
	}

}
