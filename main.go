package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/marlondev/task-api/routes"
)

func setRoutes(router *mux.Router) {
	router.HandleFunc("/tasks", routes.GetTasks).Methods("GET")
	router.HandleFunc("/tasks/{id}", routes.GetTask).Methods("GET")
	router.HandleFunc("/tasks", routes.CreateTask).Methods("POST")
	router.HandleFunc("/tasks/{id}", routes.UpdateTask).Methods("PUT")
	router.HandleFunc("/tasks/{id}", routes.DeleteTask).Methods("DELETE")
}

func main() {
	router := mux.NewRouter()

	setRoutes(router)

	log.Fatal(http.ListenAndServe(":3333", router))
}
